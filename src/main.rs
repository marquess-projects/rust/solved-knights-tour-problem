const ORDER: usize = 8;

fn display(mat: &[[u8; ORDER]]) {
    println!();
    for row in mat {
        for col in row {
            if *col == 0 {
                print!("{:2} ", "-");
            } else {
                print!("{:2} ", col);
            }
        }
        println!();
    }
    println!();
}

fn lowest_degree_adj(mat: &[[u8; ORDER]], adjs: &[(usize, usize)]) 
-> Option<(usize, usize)> {
    let order = ORDER as isize;
    let mut min_degree_count = u8::MAX;
    let mut min_degree_position = None;
    let mut degree;
    let moves = [
        (-2, 1), (-1, 2), (1, 2), (2, 1), (2, -1), (1, -2), (-1, -2), (-2, -1),
    ];
    for (row, col) in adjs {
        degree = 0;
        for &(dr, dc) in &moves {
            let new_row = *row as isize + dr;
            let new_col = *col as isize + dc;
            if new_row >= 0 && new_row < order && new_col >= 0 && new_col < order {
                let new_row = new_row as usize;
                let new_col = new_col as usize;
                if mat[new_row][new_col] == 0 {
                    degree += 1;
                }
            }
        }
        if degree < min_degree_count {
            min_degree_count = degree;
            min_degree_position = Some((*row, *col));
        }
    }

    return min_degree_position;
}

fn next_position(mat: &[[u8; ORDER]], pos: (usize, usize)) 
-> Option<(usize, usize)> {
    let order = ORDER as isize;
    let (row, col) = pos;
    let mut adjs: Vec<(usize, usize)> = Vec::new();
    if !(row < ORDER && col < ORDER) {
        return None;
    }
    let moves = [
        (-2, 1), (-1, 2), (1, 2), (2, 1), (2, -1), (1, -2), (-1, -2), (-2, -1),
    ];
    for (r, c) in moves {
        let new_row = row as isize + r;
        let new_col = col as isize + c;
        if new_row >= 0 && new_row < order && new_col >= 0 && new_col < order {
            let new_row = new_row as usize;
            let new_col = new_col as usize;
            if mat[new_row][new_col] == 0 {
                adjs.push((new_row, new_col));
            }
        }
    }
    if adjs.is_empty() {
        return None;
    }
    return lowest_degree_adj(mat, &adjs);
}

fn display_knights_tour(mat: &mut [[u8; ORDER]], pos: (usize, usize), seq_cnt: u8) 
-> bool {
    let (row, col) = pos;
    mat[row][col] = seq_cnt;
    if seq_cnt == (ORDER * ORDER) as u8 {
        display(&mat);
        return true;
    }
    if let Some(next_pos) = next_position(mat, pos) {
        display(&mat);
        return display_knights_tour(mat, next_pos, seq_cnt + 1);
    }
    false
}

fn get_position(is_row: bool) -> usize {
    use std::io::Write;
    let text = if is_row { "Linha" } else { "Coluna" };
    let mut input_num: i32;
    loop {
        print!("{}: ", text);
        let mut input: String = String::new();
        std::io::stdout().flush().expect("Error: failed to flush.");
        std::io::stdin()
            .read_line(&mut input)
            .expect("ENTRADA INVÁLIDA");
        input_num = match input.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("NÚMERO INVÁLIDO");
                continue;
            }
        };
        if input_num < 0 || input_num >= ORDER as i32 {
            println!(
                "{} MENOR OU MAIOR QUE O LIMITE ESPERADO [0 a {ORDER}]",
                text
            )
        }
        else { break; }
    }
    return input_num as usize;
}


fn main() {
    let row = get_position(true);
    let col = get_position(false);
    let mut matrix: [[u8; ORDER]; ORDER] = [[0; ORDER]; ORDER];
    display_knights_tour(&mut matrix, (row, col), 1);

    let mut wait = String::new();
    std::io::stdin().read_line(&mut wait).expect("JUST TO HOLD CONSOLE WINDOW.");
}
